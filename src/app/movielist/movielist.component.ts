import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MovieService } from '../service/movie.service';

@Component({
  selector: 'app-movielist',
  templateUrl: './movielist.component.html',
  styleUrls: ['./movielist.component.css']
})
export class MovielistComponent implements OnInit {
  @Input() favorite = [];
  @Output() favoriteAdd = new EventEmitter<[]>();

  @Input() counter;
  @Output() counterSum = new EventEmitter<number>();
  private saveFavorite: any;
  filter = "";
  movieList = [];
  constructor(private movie : MovieService,) {
    
  }

  ngOnInit() {
  }

  search (filter) {
    //console.log(filter);
    this.movie.search(filter).subscribe(
      (response) => {
          this.movieList = response['results'];
      }
    )
    //console.log(this.movieList);
  }

  addNewFavorite(image, title, resume){
    //console.log(title);
    this.saveFavorite = {image, title, resume};
    // @ts-ignore
    this.favoriteAdd.emit(this.favorite.push(this.saveFavorite));
    this.counterSum.emit(this.counter++);
    console.log(this.counter);
    //console.log(this.favorite);
  }

  

}
